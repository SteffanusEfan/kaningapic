<?php

class controller
{
	var $css;
	var $js;
	
	public function __construct()
	{
		$this->css = array();
		$this->js = array();

		if (session_status() == PHP_SESSION_NONE) {
		    session_start();
		}
				
	}
	
	protected function getView($filename, $variable)
	{
		extract($variable);
		ob_start();
	    (include $filename);
		$content = ob_get_contents();
		ob_end_clean ();
		return $content;
	}

	protected function getPage()
	{
	    $page = 1;
		if(isset($_GET['page']))
			$page = $_GET['page'];
		
		if(strlen(trim($page)) > 0)
		{
		    $page = intval($page);
		}
		else {
		    $page = 1;
		}
		return $page;
	}
            
    
	protected function redirect($page)
	{
		header( 'Location: '.BASE_URL.$page ) ;
	}
	
	protected function render($group, $view, $var)
	{
		$css[] = "/public/css/jquery-ui.css";	
		$css[] = "/public/css/gozha-nav.css";
		$css[] = "/public/css/external/jquery.selectbox.css";  
		$css[] = "/public/rs-plugin/css/settings.css"; 
		$css[] = "/public/css/style.css?v=1"; 
		$css[] = "/public/css/custom.css";
		$css[] = "/public/jcarousellite/style/style-demo.css";
		
		//--------------css untuk report-----------------------------
		$css[] = "/public/css/report/bootstrap.min.css";
		$css[] = "/public/css/report/datepicker.css"; 
		$css[] = "/public/css/report/select2.css";
		$css[] = "/public/css/report/custom.css"; 
		$css[] = "/public/css/report/font-awesome.css";
		
		//$css[] = "/public/css/report/style.css";
		//----------------end css--------------------------------
		
		$font[] = "http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css";
		$font[] = "http://fonts.googleapis.com/css?family=Roboto:400,100,700";
		$font[] = "http://fonts.googleapis.com/css?family=Open+Sans:800italic";
		
		//--------------------js untuk report-----------------------
		$js[] = "/public/js/report/jquery-2.1.3.min.js";
		$js[] = "/public/js/report/bootstrap.min.js";
		$js[] = "/public/js/report/ie10-viewport-bug-workaround.js";
		$js[] = "/public/js/report/bootstrap-datepicker.js";
		$js[] = "/public/js/report/select2.js";
		$js[] = "/public/js/report/custom.js";
		//-----------------------end js -----------------------------
		
		$js[] = "/public/js/jquery.min.js";
		$js[] = "/public/js/jquery-ui.js";
		$js[] = "/public/js/bootstrapt.min.js";
		$js[] = "/public/js/external/modernizr.custom.js";
		$js[] = "/public/js/external/jquery-migrate-1.2.1.min.js";		
		$js[] = "/public/rs-plugin/js/jquery.themepunch.plugins.min.js";
		$js[] = "/public/rs-plugin/js/jquery.themepunch.revolution.js";
		$js[] = "/public/js/jquery.mobile.menu.js";
		$js[] = "/public/js/external/jquery.selectbox-0.2.min.js";
		$js[] = "/public/js/external/jquery.raty.js";
		$js[] = "/public/js/external/form-element.js";
		$js[] = "/public/js/form.js";
		$js[] = "/public/js/controller/numbervalid.js";
		$js[] = "/public/js/ambilkota.js";
		
		//$js[] = "/public/js/external/twitterfeed.js";
		//$js[] = "/public/js/custom.js";
		
		//$var = array_merge($var, $a);
		/*
		$pathimage = "/public/images/banners/";
				$classheadermenu = "";
				$urlpage="$_SERVER[REQUEST_URI]";
				if($urlpage == "/welcome/index" || $urlpage == "/")
				{
					$classheadermenu = "header-wrapper header-wrapper--home";
				}
				else
				{
					$classheadermenu = "header-wrapper header-wrapper--home";
				}
				
				$var['classheadermenu'] = $classheadermenu;
						
				//$footer = $this->getView(DOCVIEW.'template/footer.php', array());
				//$var['footer'] = $footer;
				
				$db = Db::init();
				$preference = $db->preferences;
				$banner = $db->banners;
				$databanner = $banner->findOne();
				$datapreference = $preference->findOne();
				
				$image="";
				if (isset($datapreference['filename']))
				{
					if(strlen(trim($datapreference['filename'])) > 0)
					{
						$path_parts = pathinfo($datapreference['filename']);
						$f = $path_parts['filename'];
						$ext = $path_parts['extension'];
						$url = $f.".f120x120.".$ext;
						$image= CDN.'image/'.$url;
					}
				}
				
				
				$bannerimage=$databanner['image'];
				
				$imagebanner="";
				if (isset($bannerimage))
				{
					if(strlen(trim($bannerimage)) > 0)
					{
						$path_parts = pathinfo($bannerimage);
						$f = $path_parts['filename'];
						$ext = $path_parts['extension'];
						$url = $f.".1600x900.".$ext;
						$imagebanner= CDN.'image/'.$url;
					}
				}
				
				$var['bannerimage'] = $imagebanner;
				
				$bannertop = $this->getView(DOCVIEW.'template/banner.php', $var);
				$var['bannertop'] = $bannertop;			
				
				$loginform = $this->getView(DOCVIEW.'template/form/login.php',$var);
				$var['loginform'] = $loginform;
				
				$registerform = $this->getView(DOCVIEW.'template/form/register.php',$var);
				$var['registerform'] = $registerform;		
				
				$logo = $image;
				$var['logo'] = $logo;
				$var['controller'] = $group;*/
		
		
		$menuatas = $this->getView(DOCVIEW.'template/header_menu.php', $var);
		$var['menuatas'] = $menuatas;	
		
		$controller = $group;
		
		$content = $this->getView(DOCVIEW.$view, $var);
		
		
		$css = array_merge($css, $this->css);
		$js = array_merge($js, $this->js);
		if(strpos($_SERVER['HTTP_HOST'], 'dev') !== false)
			include(DOCVIEW."template/template.php");
		else
			include(DOCVIEW."template/maintenance/comingsoon.php");
	}
	
	protected function underrender($group,$view,$var)
	{
		include(DOCVIEW."under/index.php");
	}

}
?>