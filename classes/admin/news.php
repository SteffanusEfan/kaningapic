<?php

class news_admin extends admin {
	
	public function index() {
		$page = $this->getPage();
		$sort = $this->getSort();
		$tipesort = $this->getSortType();
		
		$limit = 10;
		$skip = (int)($limit * ($page-1));
		
		$search = isset($_SESSION['search_adminnews']) ? $_SESSION['search_adminnews'] : '';
		
		$arr = array();
		if(!empty($_POST)) {
			$search = trim($_POST['search']);			
			
			$_SESSION['search_adminnews'] = $search;
		}
		
		$query = array();
		if(strlen(trim($search)) > 0) {
			$regex = new MongoRegex('/'.$search.'/i');
			$query = array(
				'$or' => array(
				array('title' => $regex)
			));			
		}
		
		$db = Db::init();
		$col = $db->news;
		
		$data = $col->find($query)->limit($limit)->skip($skip)->sort($this->setSort($sort));
		$count = $col->count($query);
		
		$pg = new Pagination();
		$pg->pag_url = '/admin/news/index?page=';
		$pg->calculate_pages($count, $limit, $page);	
		
		$var = array(
			'sort' => $sort,
			'tipesort' => $tipesort,
			'data' => $data,
			'page' => $page,
			'judul' => ' News List',
			'search' => $search,
			'link' => '/admin/news/index',
			'pagination' => $pg->Show()
		);

		$this->js[] = '/public/backend/controller/delete.js';

		$this->render("news", "admin/news/index.php", $var);
	}

	public function add() {
		$error = array();
		$title = '';
		$highlight = '';
		$content = '';
		$shownas = 'highlight';
		
		if(!empty($_POST)) {
			$title = isset($_POST['title']) ? trim($_POST['title']) : '';
			$highlight = isset($_POST['highlight']) ? trim($_POST['highlight']) : '';
			$content = isset($_POST['content']) ? trim($_POST['content']) : '';
			$shownas = isset($_POST['shownas']) ? trim($_POST['shownas']) : $shownas;
			
			$validator = new Validator();
			$validator->addRule('title', array('require'));
			$validator->addRule('shownas', array('require'));
			$validator->addRule('content', array('require'));
			
			$setdata = array(
				'title' => $title,
				'shownas' => $shownas,
				'content' => $content
			);
			
			if($shownas == 'highlight') {
				$validator->addRule('highlight', array('require'));
				$setdata['highlight'] = $highlight;
			}

			if($validator->isValid()) {
				$data = array(
					'title' => $title,
					'shownas' => $shownas,
					'highlight' => $highlight,
					'content' => $content,
					'created_by' => trim($_SESSION['userid']),
					'time_created' => time()
				);
				
				$db = Db::init();
				$col = $db->news;
				$col->insert($data);
				
				$this->redirect('/admin/news/index');
				exit;
			}
			else
				$error = $validator->getErrors();
		}
		
		$var = array(
			'error' => $error,
			'title' => $title,
			'shownas' => $shownas,
			'highlight' => $highlight,
			'content' => $content,
			'link' => '/admin/news/add',
			'judul' => ' Add News Data'
		);
		
		$this->css[] = '/public/backend/plugins/select2/select2.css';
		$this->css[] = '/public/backend/plugins/cleditor/jquery.cleditor.css';
		
		$this->js[] = '/public/backend/plugins/select2/select2.min.js';
		$this->js[] = '/public/backend/plugins/cleditor/jquery.cleditor.js';				
		$this->js[] = '/public/backend/controller/news.js';
		
		$this->render('news', 'admin/news/add.php', $var);
	}

	public function edit() {
		$page = $this->getPage();
		$id = $_GET['id'];
		$db = Db::init();
		$col = $db->news;
		$mcol = $col->findone(array('_id' => new MongoId($id)));
		
		$error = array();
		$title = isset($mcol['title']) ? trim($mcol['title']) : '';
		$highlight = isset($mcol['highlight']) ? trim($mcol['highlight']) : '';
		$content = isset($mcol['content']) ? trim($mcol['content']) : '';
		$shownas = isset($mcol['shownas']) ? trim($mcol['shownas']) : 'highlight';
		
		if(!empty($_POST)) {
			$title = isset($_POST['title']) ? trim($_POST['title']) : '';
			$highlight = isset($_POST['highlight']) ? trim($_POST['highlight']) : '';
			$content = isset($_POST['content']) ? trim($_POST['content']) : '';
			$shownas = isset($_POST['shownas']) ? trim($_POST['shownas']) : $shownas;
			
			$validator = new Validator();
			$validator->addRule('title', array('require'));
			$validator->addRule('shownas', array('require'));
			$validator->addRule('content', array('require'));
			
			$setdata = array(
				'title' => $title,
				'shownas' => $shownas,
				'content' => $content
			);
			
			if($shownas == 'highlight') {
				$validator->addRule('highlight', array('require'));
				$setdata['highlight'] = $highlight;
			}

			if($validator->isValid()) {
				$data = array(
					'title' => $title,
					'shownas' => $shownas,
					'highlight' => $highlight,
					'content' => $content
				);
				
				$col->update(array('_id' => new MongoId($id)), array('$set' => $data));
				
				$this->redirect('/admin/news/index?page='.$page);
				exit;
			}
			else
				$error = $validator->getErrors();
		}
		
		$var = array(
			'error' => $error,
			'title' => $title,
			'shownas' => $shownas,
			'highlight' => $highlight,
			'content' => $content,
			'link' => '/admin/news/edit?id='.$id.'&page='.$page,
			'judul' => ' Edit News Data'
		);
		
		$this->css[] = '/public/backend/plugins/select2/select2.css';
		$this->css[] = '/public/backend/plugins/cleditor/jquery.cleditor.css';
		
		$this->js[] = '/public/backend/plugins/select2/select2.min.js';
		$this->js[] = '/public/backend/plugins/cleditor/jquery.cleditor.js';				
		$this->js[] = '/public/backend/controller/news.js';
		
		$this->render('news', 'admin/news/add.php', $var);
	}

	public function delete() {
		$page = $this->getPage();
		$id = $_GET['id'];
		
		$db = Db::init();
		$col = $db->news;
		$col->remove(array('_id' => new MongoId($id)));
		
		$this->redirect('/admin/news/index?page='.$page);
		exit;
	}
}
