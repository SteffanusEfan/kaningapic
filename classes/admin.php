<?php

class admin
{
	var $css;
	var $js;
	var $playlist = array();
	var $dta;
	
	public function __construct()
	{
		$this->css = array();
		$this->js = array();
		//MongoSession::init();
		//if (session_status() == PHP_SESSION_NONE)
			//session_start();
		
		/*if(!isset($_SESSION['userid']))
		{
			header( 'Location: /login' ) ;
			exit;
		}*/
						
		/*if(!isset($_SESSION['email']))
		{
			header( 'Location: /welcome/err404' ) ;
			exit;
		}*/
	}
	
	protected function before()
	{
		
	}
	
	protected function getView($filename, $variable)
	{
		extract($variable);
		ob_start();
		(include $filename);
		$content = ob_get_contents();
		ob_end_clean ();
		return $content;
	}

	protected function getPage()
	{
	    $page = 1;
		if(isset($_GET['page']))
			$page = $_GET['page'];
		
		if(strlen(trim($page)) > 0)
		{
		    $page = intval($page);
		}
		else {
		    $page = 1;
		}
		return $page;
	}
	
	protected function getSort()
	{
	    $sort = "time_created";
		if(isset($_GET['sort']))
			$sort = $_GET['sort'];
		
		return $sort;
	}
	
	protected function getSortType()
	{
		$tipesort = 'ASC';
		if (isset($_GET['tipesort'])){
		
			if($_GET['tipesort'] == "ASC")
				$tipesort = 'ASC';
			else
				$tipesort = 'DESC';
		}
		
		return $tipesort;
	}
	
	protected function setSort($sort)
	{
		if(strlen($sort) < 1)
			$sort = 'time_created';
		
		$setsort = array($sort => -1);
		if (isset($_GET['tipesort']))
		{
			if($_GET['tipesort'] == "ASC")
				$setsort = array($sort => 1);
			else
				$setsort = array($sort => -1);
		}
		return $setsort;
	}
            
    protected function getListDevice($page=1,$limit=20,$url='')
	{
		$db = Db::init();
		$devTbl = $db->devices;

        	$skip = (int)($limit * ($page - 1));
		
		$count = $devTbl->count();

		$devList = $devTbl->find()->limit($limit)->skip($skip);
		$pg = new Pagination();
		$pg->pag_url = $url;
		$pg->calculate_pages($count, $limit, $page);
		$var = array(
			'deviceList' => $devList,
			'pagination' => $pg->Show()
		);
		return $var;
	}
	
	protected function redirect($page)
	{
		header( 'Location: '.$page ) ;
	}
	
	protected function render($group, $view, $var)
	{		
		$css[] = '/public/backend/plugins/colorpicker/colorpicker.css';
		$css[] = '/public/backend/custom-plugins/wizard/wizard.css';
		$css[] = '/public/backend/bootstrap/css/bootstrap.min.css';
		$css[] = '/public/backend/css/fonts/ptsans/stylesheet.css';
		$css[] = '/public/backend/css/fonts/icomoon/style.css';
		$css[] = '/public/backend/css/mws-style.css';
		$css[] = '/public/backend/css/icons/icol16.css';
		$css[] = '/public/backend/css/icons/icol32.css';
		$css[] = '/public/backend/css/demo.css';
		$css[] = '/public/backend/jui/css/jquery.ui.all.css';
		$css[] = '/public/backend/jui/jquery-ui.custom.css';
		$css[] = '/public/backend/css/mws-theme.css';
		$css[] = '/public/backend/css/themer.css';
		
		$js[] = '/public/backend/js/libs/jquery-1.8.3.min.js';					
		$js[] = '/public/backend/js/libs/jquery.mousewheel.min.js';
		$js[] = '/public/backend/js/libs/jquery.placeholder.min.js';
		$js[] = '/public/backend/custom-plugins/fileinput.js';
		
		//jQuery-UI Dependent Scripts
		$js[] = '/public/backend/jui/js/jquery-ui-1.9.2.js';
		$js[] = '/public/backend/jui/jquery-ui.custom.min.js';
		$js[] = '/public/backend/jui/js/jquery.ui.touch-punch.js';
		
		//Plugin Scripts
		$js[] = '/public/backend/plugins/datatables/jquery.dataTables.min.js';
		$js[] = '/public/backend/plugins/flot/jquery.flot.min.js';		
		$js[] = '/public/backend/plugins/flot/plugins/jquery.flot.tooltip.min.js';
		$js[] = '/public/backend/plugins/flot/plugins/jquery.flot.pie.min.js';		
		$js[] = '/public/backend/plugins/flot/plugins/jquery.flot.stack.min.js';
		$js[] = '/public/backend/plugins/flot/plugins/jquery.flot.resize.min.js';		
		$js[] = '/public/backend/plugins/colorpicker/colorpicker-min.js';
		$js[] = '/public/backend/plugins/validate/jquery.validate-min.js';		
		$js[] = '/public/backend/custom-plugins/wizard/wizard.min.js';
		$js[] = '/public/backend/plugins/autosize/jquery.autosize.min.js';
		$js[] = '/public/backend/controller/autosize.js';
		
		//Core Script
		$js[] = '/public/backend/bootstrap/js/bootstrap.min.js';		
		$js[] = '/public/backend/js/core/mws.js';

		
		$m = explode("/", $view);
		$mm = explode(".", $m[1]);
		$method = $mm[0];
		
		$leftmenu = $this->getView(DOCVIEW."admin/template/leftmenu.php", array('controller' => $group, 'method' => $method));
				
		//$a["shorcut"] = $shorcut;
		//$var = array_merge($var, $a);
		//$var = array_merge($var, $this->dta);
		//echo DOCVIEW.$view;
		//echo die('masuk render');
		//print_r($var);
		//die;
		$content = $this->getView(DOCVIEW.$view, $var);		
		$controller = $group;
		
		$css = array_merge($css, $this->css);
		$js = array_merge($js, $this->js);	
		include(DOCVIEW."admin/template/template.php");
	}

	protected function authorizedMethod($method)
    {
        $db = Db::init();   
        $usr = $db->users;
        $role = $db->roles;
        $dtausr = $usr->findOne(array('_id' => new MongoId ($_SESSION['user_id'])));
        $func = array();
        if(isset($dtausr['roles_id']))
        {
            $rl = $role->findOne(array('_id' => new MongoId ($dtausr['roles_id'])));
            $no = 0;
            foreach($rl['controller'] as $cc)
            {                                            
                foreach($cc['method'] as $ss)
                {   
                    if($ss['status'] == 'yes')               
                        array_push($func, $ss['name']);
                }
            }
        }
        
        $bouncer = new Bouncer();
        $bouncer->addRole(trim($dtausr['roles_id']), $func);
        $this->auth->addRole(trim($dtausr['roles_id']));
        if(!$bouncer->verifyAccess($this->auth->getRoles(), $method))
        {
            header( 'Location: /error/err401' ) ;
			exit;
        }
    }
}
?>