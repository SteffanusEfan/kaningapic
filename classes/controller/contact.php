<?php
	class contact_controller extends controller{
		
		public function index(){
			$db = Db::init();
			$colcontact = $db->preference;
			
			$datacontact = $colcontact->find();
			
			$var = array(
				'datacontact' => $datacontact
			);
			
			$this->render('contact', "/contact/index.php", $var);
		}
	}
?>