<?php
	class film_controller extends controller{
		
		public function index(){
			$db = Db::init();
			$colfilm = $db->movie;
			
			$datafilm = $colfilm->find();
			
			$var = array(
				'datafilm' => $datafilm
			);
			
			$this->render('film', "/film/index.php", $var);
			
		}
	}
?>