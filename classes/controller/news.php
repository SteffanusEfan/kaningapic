<?php 
	class news_controller extends controller{
		
		public function index(){
			$db = Db::init();
			$colnews = $db->news;
			
			$datanews = $colnews->find();
			
			$var = array(
				'datanews' => $datanews
			);
			
			$this->render('news', "/news/index.php", $var);
		}
	}
?>