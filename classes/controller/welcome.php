<?php

class welcome_controller extends controller
{
	public function index()
	{
		$db = Db::init();
		$colpref = $db->preference;
		$colslider = $db->slider;
		
		$datapref = $colpref->find();
		$dataslider = $colslider->find();
		
		$var = array(
			'datapref' => $datapref,
			'dataslider' => $dataslider
		);
		
		$this->render('home', 'welcome/index2.php', $var);
	}
	
	public function info()
	{
		echo phpinfo();
	}
	
	public function test()
	{
		echo "test";
	}
}
