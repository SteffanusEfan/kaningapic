<?php

	class about_controller extends controller{
		
		public function index(){
			$db = Db::init();
			$colabout = $db->about;
			
			$dataabout = $colabout->find();
			
			$var = array(
				'dataabout' => $dataabout
			);
			
			$this->render('about', "/about/index.php", $var);
		}
	}
	
?>