<?php
date_default_timezone_set("Asia/Jakarta");
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);
define('DOCROOT', realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR);
define('BASEPATH', realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR);
define('DOCVIEW', DOCROOT."view".DIRECTORY_SEPARATOR); 
define('BASE_URL', "http://".$_SERVER['HTTP_HOST']);
//define('SITEKEY', 'plokijuhygtfrdeswaq');
define('IMGPATH', '/var/www/upload/');
define('CACHE', "/tmp/cache/");
//define('CDN', "http://cdn.pos.dboxid.com/");

session_start();
include_once(DOCROOT."lib/chilkat_9_5_0.php");

function __autoload($class_name) {
	//echo $class_name.'<br/>';
	$s = explode("_", $class_name);	
	//print_r($s);
	if(count($s) == 1)
	{
		if(file_exists(DOCROOT."/classes/".$class_name . '.php'))
			include DOCROOT."/classes/".$class_name . '.php';
		else if(file_exists(DOCROOT."/lib/".$class_name . '.php'))
			include DOCROOT."/lib/".$class_name . '.php';
	}
	else {
		if(file_exists(DOCROOT."/classes/".$s[1]."/".$s[0] . '.php'))
			include DOCROOT."/classes/".$s[1]."/".$s[0] . '.php';
		elseif(file_exists(DOCROOT."/lib/".$s[0] . '.php'))
			include DOCROOT."/lib/".$s[0]. '.php';
	}
}

$path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
if($path == "/")
{
	$r = new welcome_controller(false);
	$r->index();
}
else 
{	
	$adacontent = false;
	$pp = explode("/", $path);
	
	if(count($pp) > 2)
	{
		if (class_exists($pp[1].'_controller')) {
			$rr = $pp[1].'_controller';
			$r = new $rr();
			if(method_exists($r, $pp[2]))
			{
				$r->$pp[2]();
				$adacontent = true;
			}
			else {
				if($rr == 'news_controller')
				{
					if(isset($pp[2]))
					{
						$r->detail($pp[2]);
						$adacontent = true;
					}
				}
				
			}
		}		
		if(count($pp) > 3)
		{
			if($pp[1] == "admin") {
				if (class_exists($pp[2].'_admin')) 
				{
					$rr = $pp[2].'_admin';
					$r = new $rr();
					if(method_exists($r, $pp[3]))
					{
						$r->$pp[3]();
						$adacontent = true;
					}
				}
			}
		}
	}
	else {		
		$rr = $pp[1].'_controller';
		$r = new $rr();
		if(method_exists($r, "index"))
		{
			$r->index();
			$adacontent = true;
		}
		
	}
	if(!$adacontent)
	{
		//$r = new welcome_controller();
		//$r->err404();
	}
}	
