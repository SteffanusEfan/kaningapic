<?php

foreach($js as $j)
{
?>

	<script src="<?php echo $j?>"></script>

<?php	
}

?>

 <!-- Slider -->
<div class="bannercontainer">
    <div class="banner">
        <ul>
        	<?php
        	foreach ($dataslider as $dsb) 
        	{
        		$image="";
				if (isset($dsb['filename']))
				{
					if(strlen(trim($dsb['filename'])) > 0)
					{
						$path_parts = pathinfo($dsb['filename']);
						$f = $path_parts['filename'];
						$ext = $path_parts['extension'];
						$url = $f.".".$ext;
						$image= CDN.'image/'.$url;
					}
				}
        		
        	?>
                <li data-transition="fade" data-slotamount="7" class="slide" data-slide='<?php echo $dsb['title']?>'>
                    <img class="img-slide" alt='' src="<?php echo $image?>" >
                    <div class="caption slide__name margin-slider" 
                         data-x="right" 
                         data-y="80" 

                         data-splitin="chars"
                         data-elementdelay="0.1"

                         data-speed="700" 
                         data-start="1400" 
                         data-easing="easeOutBack"

                         data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"

                         data-frames="{ typ :lines;
                                     elementdelay :0.1;
                                     start:1650;
                                     speed:500;
                                     ease:Power3.easeOut;
                                     animation:x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:1;transformPerspective:600;transformOrigin:50% 50%;
                                     },
                                     { typ :lines;
                                     elementdelay :0.1;
                                     start:2150;
                                     speed:500;
                                     ease:Power3.easeOut;
                                     animation:x:0;y:0;z:0;rotationX:00;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:1;transformPerspective:600;transformOrigin:50% 50%;
                                     }
                                     "


                        data-splitout="lines"
                        data-endelementdelay="0.1"
                        data-customout="x:-230;y:0;z:0;rotationX:0;rotationY:0;rotationZ:90;scaleX:0.2;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%"

                        data-endspeed="500"
                        data-end="8400"
                        data-endeasing="Back.easeIn"
                         >
                        <?php echo $dsb['title']?>
                    </div>
                 
                    <div class="caption slide__text margin-slider customin customout" 
                         data-x="right" 
                         data-y="250"
                         data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 
                         data-speed="400" 
                         data-start="3000"
                         data-endspeed="400"
                         data-end="8000"
                         data-endeasing="Back.easeIn">
                        <?php echo $dsb['description']?>
                     </div>
                    <div class="caption margin-slider skewfromright customout " 
                         data-x="right" 
                         data-y="324"
                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-speed="400" 
                         data-start="3300"
                         data-easing="Power4.easeOut"
                         data-endspeed="300"
                         data-end="7700"
                         data-endeasing="Power4.easeOut">
                         <!--a href="<?php //echo $dsb['link']?>" onclick="test('<?php //echo $dsb['link']?>')"  class="btn btn-md btn--danger btn--wide slider--btn">detail film</a-->
                     </div>
                </li>
           <?php
           }
           ?> 
        </ul>
    </div>
 </div>

<!--end slider -->