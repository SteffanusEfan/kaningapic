<div id="detail-nowplaying" class="col-sm-8 col-md-8 body-detail">
	<div class="col-md-6">
		<div class="col-md-9">
			<h6 class="bold"><?php echo $data['name']; ?></h6>
			<ul class="summary">
				<li>
					<span class="key">genre</span>
					<?php
					$db = Db::init();
					$g = $db->genres;
					$genre = '&nbsp;';
					if(isset($data['genre'])) {
						$gg = $g->findone(array('_id' => new MongoId($data['genre'])));
						$genre = $gg['name'];
					}
					?>
					<span class="val"><?php echo $genre; ?></span>
				</li>
				<li>
					<span class="key">produser</span>
					<span class="val"><?php echo isset($data['produser']) ? $data['produser'] : '&nbsp;'; ?></span>
				</li>
				<li>
					<span class="key">sutradara</span>
					<span class="val"><?php echo isset($data['sutradara']) ? $data['sutradara'] : '&nbsp;'; ?></span>
				</li>
				<li>
					<span class="key">penulis</span>
					<span class="val"><?php echo isset($data['penulis']) ? $data['penulis'] : '&nbsp;'; ?></span>
				</li>
				<li>
					<span class="key">produksi</span>
					<span class="val"><?php echo isset($data['produksi']) ? $data['produksi'] : '&nbsp;'; ?></span>
				</li>
			</ul>
			<h6 class="bold">CAST</h6>
			<?php
				if(isset($data['cast'])) {
					if(strlen(trim($data['cast'])) > 0) {
						$e = explode(',', $data['cast']);
						echo '<ul class="cast">';
						foreach($e as $ee) {
							if(strlen(trim($ee)) > 0) {
								echo '<li><span class="desc">'.$ee.'</span></li>';
							}									
						}
						echo '</ul>';
					}
				}
			?>
							
		</div>
		<div class="col-md-3 duration">
			<h5 class="bold"><?php echo isset($data['duration']) ? helper::timetosecond($data['duration']) : '0'; ?></h5>
			<p style="margin-top: -20px; text-align: center;" class="synopsis">Minutes</p>
		</div>
		<div class="clearfix"></div>
		<div class="col-md-12 margin10">
			<h6 class="bold">SYNOPSIS</h6>
			<p class="synopsis"><?php echo isset($data['synopsis']) ? helper::limitString($data['synopsis']) : '&nbsp'; ?></p>
			<ul class="body-detail-menu">
				<li><a class="bold" href="/trailer/index?id=<?php echo $data['_id']; ?>">trailer</a></li>
				<li><a class="bold" href="/booking/index?id=<?php echo $data['_id']; ?>">buy ticket</a></li>
			</ul>
		</div>			
	</div>
	<div class="col-md-6">
		<?php
		if (isset($data['image']))
		{
			if(strlen(trim($data['image'])) > 0)
			{
				$path_parts = pathinfo($data['image']);
				$f = $path_parts['filename'];
				$ext = $path_parts['extension'];
				$url = $f.".".$ext;
				$image= CDN.'image/'.$url;
				echo '<img style="margin-left:10px" width="100%" src="'.$image.'" alt=""/>';
			}
		}
		?>
	</div>
</div>