<ul>		            	
<?php
$db = Db::init();
$schedule = $db->schedules;
$movie = $db->movies;
$mv = $movie->find();
$n=0;
foreach($data as $dts) {
	echo '<li id="accordion">';
	echo '<a style="padding-left: 0;" data-nmr="'.$n.'" data-toggle="collapse" data-parent="#accordion" href="#collapse'.$n.'" class="studio collapsed">'.$dts['subdistrict'].'</a>';
	echo '</li>';
	echo '<div style="margin-bottom: 20px" id="collapse'.$n.'" class="panel-collapse collapse">';
	echo '<p style="font-size: 11px; margin-bottom: 0; margin-top:-15px;color: #86736b;">'.$dts['name'].'</p>';
	$q = array(
		'studio' => trim($dts['_id']),
		'scheduledate' => time()
	);
	$ss = $schedule->find($q);
	foreach($mv as $dv) {
		$ada = false;
		$jadwal = '';
		foreach($ss as $ds) {
			if($dv['_id'] == $ds['movie']) {
				$ada = true;
				$jadwal .= '<p style="text-transform: uppercase; font-size:12px; margin-bottom: -20px; line-height: 40px; padding-left: 10px;color: #86736b;">'.date('F d', $ds['scheduledate']).' - '.$ds['scheduletime'].' WIB</p>';
			}
		}
		if($ada) {
			echo '<a id="film" data-id="'.$dv['_id'].'" style="text-transform: uppercase; cursor: pointer; padding-left: 10px;">'.$dv['name'].'</a>';
			if (isset($dv['image']))
			{
				if(strlen(trim($dv['image'])) > 0)
				{
					$path_parts = pathinfo($dv['image']);
					$f = $path_parts['filename'];
					$ext = $path_parts['extension'];
					$url = $f.".80x120.".$ext;
					$image= CDN.'image/'.$url;
					echo '<img style="margin-left:10px" width="80" height="120" src="'.$image.'" alt=""/>';
				}
			}									
			echo $jadwal;
		}
	}
	echo '</div>';		            	 	
	$n++;							
}
?>		            	
</ul>