<div class="container">

  <form class="form-signin" method="post" action="/akun/login">
    <h2 class="form-signin-heading">Kaninga Cinema</h2>
    <label for="inputEmail" class="sr-only">Email address</label>
    <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" name="pass" id="pass" class="form-control" placeholder="Password" required>
    
    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    
  </form>
</div>