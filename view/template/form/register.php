<div class="overlay-register overlay-hugeinc">
            
            <section class="container">

                <div class="col-sm-4 col-sm-offset-4">
                    <button type="button" class="overlay-close-register">Close</button>
                    <form id="login-form" class="login">
                        <p class="login__title">Daftar<br><span class="login-edition">welcome to Kaninga Cinema</span></p>

                        <div class="social social--colored">
                                <a href='#' class="social__variant fa fa-facebook"></a>
                                <a href='#' class="social__variant fa fa-twitter"></a>
                                <a href='#' class="social__variant fa fa-tumblr"></a>
                        </div>

                        <p class="login__tracker">or</p>
                        
                        <div class="alert alert-danger error-log"></div>
                        
                        <div class="field-wrap">
	                        <input type='text' placeholder='Nama' name='name' class="login__input" id="name">
	                        <input type='email' placeholder='Email' name='email' class="login__input" id="email">
	                        <input type='password' placeholder='Password' name='password' class="login__input" id="password">
                         </div>
                        
                        <div class="login__control">
                            <a href='javascript:;' class="btn btn-md btn--warning btn--wider" id="register" link="<?php echo $_SERVER['REQUEST_URI']; ?>">daftar</a>
                        </div>
                        
                        <div class="alert alert-success">
 						 	<strong>Success</strong> Akun Anda Telah Terdaftar.
						</div>
                    </form>
                </div>

            </section>
 </div>