<div class="overlay overlay-hugeinc">
            
            <section class="container">

                <div class="col-sm-4 col-sm-offset-4">
                    <button type="button" class="overlay-close">Close</button>
                    <form id="login-form" class="login" method='post' novalidate=''>
                        <p class="login__title">Login <br><span class="login-edition">welcome to Kaninga Cinema</span></p>

                        <div class="social social--colored">
                                <a href='#' class="social__variant fa fa-facebook"></a>
                                <a href='#' class="social__variant fa fa-twitter"></a>
                                <a href='#' class="social__variant fa fa-tumblr"></a>
                        </div>

                        <p class="login__tracker">or</p>
                        
                         <div class="alert alert-danger error-log"></div>
                        
                        <div class="field-wrap">                        	
                        <input type='email' placeholder='Email' name='email' class="login__input" id="emaillogin">
                        <input type='password' placeholder='Password' name='password' class="login__input" id="passwordlogin">

                        <!--<input type='checkbox' id='#informed' class='login__check styled'>
                        <label for='#informed' class='login__check-info'>remember me</label>!-->
                         </div>
                        
                        <div class="login__control">
                            <a href="javascript:;" class="btn btn-md btn--warning btn--wider" id="login" link="<?php echo $_SERVER['REQUEST_URI']; ?>">login</a>
                            <a href="#" class="login__tracker form__tracker">Lupa Password?</a>
                            <div class="alert alert-success">
 								<strong>Login Success</strong>.
							</div>
                        </div>
                    </form>
                </div>
				 
            </section>
        </div>