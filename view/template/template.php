<!doctype html>
<html>
<Head>
	<!-- Basic Page Needs -->
        <meta charset="utf-8">
        <link rel="shortcut icon" type="image/png" href="http://eg.com/favicon.png"/>
        <title>Kaninga</title>
        <meta name="description" content="A Template by Gozha.net">
        <meta name="keywords" content="HTML, CSS, JavaScript">
        <meta name="author" content="Gozha.net">
    	<link rel="icon" href="/public/images/favicon.ico">
    	<!-- Mobile Specific Metas-->
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="telephone=no" name="format-detection">   	    	    	
    	
        <!--CSS Start -->
        <?php
        	foreach($css as $datcss)
			{
				echo '<link rel="stylesheet" href="'.$datcss.'">';
			}
        ?>
        <!--CSS End -->
        
        <?php
		foreach($js as $datjs)
		{
			echo '<script src="'.$datjs.'"></script>';
		}
		?>
		<!-- End JS -->
        
        <!--Font Start -->
        <?php
        	foreach($font as $datfont)
			{
				echo '<link rel="stylesheet" href="'.$datfont.'">';
			}
        ?>
        <!--Font End -->
        
         
    
    <script  type="text/javascript" src="http://wallet.deboxs.com/public/DigiPay.js"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries --> 
    <!--[if lt IE 9]> 
    	<script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.js"></script> 
		<script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.js"></script>		
    <![endif]-->
</Head>

<Body>
    <div class="wrapper">
        <?php //echo $bannertop?>

      	<?php echo $menuatas; ?>
        
        <!-- Main content -->
        <?php echo $content; ?>
        <?php if($controller !== 'home') {?>
        <div class="clearfix"></div>

        <footer class="footer-wrapper">
            <section class="container">
            	<p class="copy">&copy; Kaninga Cinema 2016 Powered By Digitama</p>
                <!--div class="col-xs-4 col-md-2 footer-nav">
                    <ul class="nav-link">
                        <li><a href="/studio/index" class="nav-link__item">Theaters</a></li>
                        <li><a href="/movieplaying/index" class="nav-link__item">Now Playing</a></li>
                        <li><a href="/trailer/index" class="nav-link__item">Trailer</a></li>
                        <li><a href="/commingsoon/index" class="nav-link__item">Coming Soon</a></li>
                    </ul>
                </div>
                <div class="col-xs-4 col-md-2 footer-nav">
                      <ul class="nav-link">
                        <li><a href="/news/index" class="nav-link__item">News</a></li>
                    </ul>
                </div>
                <div class="col-xs-4 col-md-2 footer-nav">
                   
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="footer-info">
                        <p class="heading-special--small">Kaninga Cinema<br><span class="title-edition">social media</span></p>

                        <div class="social">
                            <a href="http://facebook.com" target="_blank" class="social__variant fa fa-facebook"></a>&nbsp;
                            <a href="http://twitter.com" target="_blank" class="social__variant fa fa-twitter"></a>&nbsp;
                            <a href='https://instagram.com' target="_blank" class="social__variant fa fa-instagram"></a>
                        </div>
                        
                        <div class="clearfix"></div>
                        <p class="copy">&copy; Kaninga Cinema, <?php echo date("Y",time())?>.Powered By Digitama</p>
                    </div>
                </div-->
            </section>
        </footer>
        <?php } ?>
    </div>

    <!-- open/close -->
    
    <!-- login -->
     <?php echo $loginform?>
     <!-- login -->
     <!-- Register -->
     <?php echo $registerform?>
     <!-- Register -->     
     <?php if($controller == 'home') { ?>
     	<script type="text/javascript">
     		$(document).ready(function() {
     			var h = $(window).height();
     			var w = $(window).width();
     			var hb = parseInt(h)-55;
     			$('.bannercontainer').css({'width':w, 'height':hb, 'top':'-55px'});
     			$('.header-wrapper').css({'top':hb, 'position':'relative'});
     			var h = $('div.wrapper').height();
	 			var hf = parseInt(h)-100;
	 			//$('footer.footer-wrapper').css({'top':hf+'px', 'height':'75px', 'padding-top':'30px'});
     		});
     	</script>
 	 <?php } else {?>
 	 	<script type="text/javascript">
     		$(document).ready(function() {
     			var h = $(window).height();
     			var w = $(window).width();
     			var hh = parseInt(h);
     			var hb = parseInt(h)-55;
     			if(hh > 600) {
     				$('section.container').css({'width':w, 'height':hb, 'top':'55px'});
	     			var hc = $('section.container').height()*0.7;
	     			$('.carousel').css({'min-height':hc+'px'});	
     			}     			
     			var h = $('div.wrapper').height();
	 			var hf = parseInt(h)-100;
	 			$('footer.footer-wrapper').css({'top':hf+'px', 'padding-top':'0px'});
     		});
     	</script>
 	<?php } ?> 	
</Body>
</html>
<!-- JS-->
<?php
echo '<script src="/public/js/custom.js"></script>';	
echo '<script src="/public/js/controller/postaccount.js"></script>';		
?>

<!-- End JS -->
