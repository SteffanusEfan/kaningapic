<!-- Header section -->
<header class="header-wrapper header-wrapper--home">
    <div class="container">
        <!-- Logo link-->
        <a href='/welcome/index' class="logo">
            <img alt='logo' src="<?php echo $logo?>" >
        </a>
        
        <!-- Main website navigation-->
        <nav id="navigation-box">
            <!-- Toggle for mobile menu mode -->
            <a href="#" id="navigation-toggle">
                <span class="menu-icon">
                    <span class="icon-toggle" role="button" aria-label="Toggle Navigation">
                      <span class="lines"></span>
                    </span>
                </span>
            </a>
            
            <!-- Link navigation -->
            <ul id="navigation" >
                 <li>
                    <span class="sub-nav-toggle plus"></span>
                    <a href="#">HOME</a>
                </li>
                <li>
                    <span class="sub-nav-toggle plus"></span>
                    <a href="#">ABOUT</a>
                </li>
                <li>
                    <span class="sub-nav-toggle plus"></span>
                    <a href="#">FILMS</a>
                </li>
                <li>
                    <span class="sub-nav-toggle plus"></span>
                    <a href="#">NEWS</a>
                </li>
                 <li>
                    <span class="sub-nav-toggle plus"></span>
                    <a href="#">CONTACT</a>
                </li>
                <li>
                    <span class="sub-nav-toggle plus"></span>
                    <a href="#">BLOG</a>
                </li>
            </ul>
           </nav>
           
           <div class="control-panel">
        <?php
        /*if(isset($_SESSION['userid']))
        {
        ?>
        	<div class="auth auth--home">
              <div class="auth__show">
                <!--<span class="auth__image">
                  <img alt="" src="http://placehold.it/31x31">
                </span>!-->
              </div>
              <a href="#" class="btn btn--sign btn--singin">
                  <?php echo $_SESSION['name'];?>
              </a>
                <ul class="auth__function">
                    <li><a href="#" class="auth__function-item">Watchlist</a></li>
                    <li><a href="#" class="auth__function-item">Booked tickets</a></li>
                    <li><a href="#" class="auth__function-item">Discussion</a></li>
                    <li><a href="#" class="auth__function-item">Settings</a></li>
                </ul>
            </div>
            <a href="/account/logout?pagename=<?php echo $_SERVER['REQUEST_URI'];?>" class="btn btn-md btn--warning btn--book btn-control--home">Keluar</a>
        <?php 
		}
		else
		{
        ?>
            <!-- Additional header buttons / Auth and direct link to booking-->
               <a href="#" class="btn btn--sign login-window">Masuk</a>
               <a href="#" class="btn btn-md btn--warning btn--book btn-control--home register-window">Daftar</a>
		<?php
		}*/			
		?>
		</div>
    </div>
</header>
