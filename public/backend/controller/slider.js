$(document).ready(function() {
	$('body').delegate('.btn-submit', 'click', function() {
		var f = $('.photo').val();
		if(f.length > 0)
			$('#form-slider').submit();
		else {
			var s = '<div class="mws-error"><p style="color: red">field required !</p></div>';
			$('.mws-error').replaceWith(s);
		}			
	});
	$('textarea.description').cleditor({ width: '100%' });
});
