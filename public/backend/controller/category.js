$(document).ready(function() {
	$('body').delegate('.btn-add-language', 'click', function() {
		$.get('/util/addCategory', function(data) {
			$('#view-category').replaceWith(data);
			$("select.mws-select2").select2();		
		});							
	});
	
	$("select.mws-select2").select2();
});
