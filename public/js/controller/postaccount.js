$( "#register" ).click(function() {
 var link = $(this).attr('link');
  	  $.post("/account/register",
        {
          name  : $("#name").val(),
          email : $("#email").val(),
          password : $("#password").val(),
        },
        
        function(data,status){
        	if(data == "SUCCESS")
        	{
        		$(".alert-success").fadeIn(500);
        		$(".alert-success").delay(500).fadeOut(500);
        		$(".alert-danger").fadeOut(500);
        		
        		window.location = link;
        	}
        	else
        	{
        		$(".error-log").empty();
	        	var obj = JSON.parse(data);
	        	var logname = obj.name;
	        	var logemail = obj.email;
	        	var logpassword = obj.password;
	        	
	            for(var errorname in logname)
	            {
	            	$(".error-log").append("<strong>Name </strong>"+ logname[errorname]+"<br>");
	            }
	            
	            for(var erroremail in logemail)
	            {
	            	$(".error-log").append("<strong>Email </strong>"+ logemail[erroremail]+"<br>");
	            }
	            
	            for(var errorpassword in logpassword)
	            {
	            	$(".error-log").append("<strong>Password </strong>"+ logpassword[errorpassword]+"<br>");
	            }
	            
	            $(".alert-danger").fadeIn(500);
	            
        	}
        });
});

$( "#login" ).click(function() {
 
 	var link = $(this).attr('link');
  	  $.post("/account/login",
        {
          email : $("#emaillogin").val(),
          password : $("#passwordlogin").val()
        },
        
        function(data,status){
        	console.log(data);
        	if(data == "SUCCESS")
        	{
        		$(".alert-success").fadeIn(500);
        		$(".alert-success").delay(500).fadeOut(500);
        		$(".alert-danger").fadeOut(500);
        		
        		window.location = link;
        	}
        	else
        	{
        		$(".error-log").empty();
	        	var obj = JSON.parse(data);
	        	var logaccount = obj.account;
	        	var logemail = obj.email;
	        	var logpassword = obj.password;
	        
	            for(var erroremail in logemail)
	            {
	            	$(".error-log").append("<strong>Email </strong>"+ logemail[erroremail]+"<br>");
	            }
	            
	            for(var errorpassword in logpassword)
	            {
	            	$(".error-log").append("<strong>Password </strong>"+ logpassword[errorpassword]+"<br>");
	            }
	             for(var erroraccount in logaccount)
	            {
	            	$(".error-log").append("<strong>Account </strong>"+ logaccount[erroraccount]+"<br>");
	            }
	            
	            $(".alert-danger").fadeIn(500);
	            
        	}
        });
});
