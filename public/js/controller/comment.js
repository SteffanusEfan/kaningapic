$(document).ready(function(){
	$('body').delegate('.comment-form__btn', 'click', function() {
		var id = $(this).attr('data-id');
		var text = $('.comment-form__text').val();
		
		if(id.length > 0 && text.length > 0) {
			$.post('/util/insertcomment', {'id':id, 'comment':text}, function(data){
				if(data != 'gagal') {
					var js = $.parseJSON(data);
					$('.comment-form__text').val('');
					$('.comment-link').html('Comment: '+js.count);
					$('.cmt').html('comment ('+js.count+')');
					$('#view-comment').replaceWith(js.data);					
				}				
				return false;
			});
			return false;
		}
	});
	
	$('.comment-form__text').bind('input', function() {
		var text = $(this).val();
		var l = text.length;
		var ll = parseInt(l);
		
		var sisa = (250-l);		
		$('.comment-form__info').text(sisa+' characters left');
		
		if(ll > 250) {
			var txt = text.substring(0,250);
			$(this).val(txt);
			$('.comment-form__info').text('0 characters left');
		}				
	});
});
