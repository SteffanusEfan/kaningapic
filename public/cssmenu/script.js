$( document ).ready(function() {
	$('#cssmenu > ul > li > a').click(function() {
		console.log('click');
	  	$('#cssmenu li').removeClass('active');
	  	console.log('removeClass active');
	  	$(this).closest('li').addClass('active');
	  	console.log('addClass active');	
	  	var checkElement = $(this).next();
	  	if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
	    	$(this).closest('li').removeClass('active');
	    	console.log('removeClass ul li active');
	    	checkElement.slideUp('normal');
	  	}
	  	if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
	    	$('#cssmenu ul ul:visible').slideUp('normal');
	    	console.log('slideUp normal');
	    	checkElement.slideDown('normal');
	  	}
	  	if($(this).closest('li').find('ul').children().length == 0) {
	  		console.log('if');
	    	return true;
	  	} else {
	  		console.log('else');
	    	return false;	
	  	}		
	});
});
